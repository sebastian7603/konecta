<?php

use Illuminate\Database\Seeder;
use App\Models\DocType;

class DocTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = new DocType();
        $type->abbr = 'CC';
        $type->name = 'CÉDULA DE CIUDADANÍA';
        $type->save();
        
        $type = new DocType();
        $type->abbr = 'CE';
        $type->name = 'CÉDULA DE EXTRANJERÍA';
        $type->save();

        $type = new DocType();
        $type->abbr = 'PAS';
        $type->name = 'PASAPORTE';
        $type->save();
    }
}

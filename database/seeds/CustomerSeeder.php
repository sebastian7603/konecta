<?php

use Illuminate\Database\Seeder;
use App\Models\Customer;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customer = new Customer();
        $customer->doc_type_id = 1;
        $customer->document = '63524192';
        $customer->names = 'EMILIO';
        $customer->lastnames = 'RÍOS CÁRDENAS';
        $customer->address = 'CALLE FALSA 1234';
        $customer->email = 'emiliorios@gmail.com';
        $customer->save();
        
        $customer = new Customer();
        $customer->doc_type_id = 2;
        $customer->document = '1098538203';
        $customer->names = 'DIANA CAROLINA';
        $customer->lastnames = 'PARRA RINCÓN';
        $customer->address = 'CALLE 11 # 76 - 12';
        $customer->email = 'dianaparra@gmail.com';
        $customer->deleted_at = date('Y-m-d H:i:s');
        $customer->save();
        
        $customer = new Customer();
        $customer->doc_type_id = 3;
        $customer->document = 'AV763952';
        $customer->names = 'XIMENA';
        $customer->lastnames = 'GONZÁLEZ BORJA';
        $customer->address = 'CARRERA 123 # 73 - 45';
        $customer->email = 'ximegonzalez@gmail.com';
        $customer->save();
    }
}

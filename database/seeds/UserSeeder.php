<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->names = 'SEBASTIÁN';
        $user->lastnames = 'AYALA SUÁREZ';
        $user->phone = '3188520728';
        $user->email = 'sebastianutpae@gmail.com';
        $user->password = Hash::make('qwerty');
        $user->save();
        $user->roles()->attach(1);

        $user = new User();
        $user->names = 'JUAN DIEGO';
        $user->lastnames = 'ALVIRA LÓPEZ';
        $user->phone = '3112345678';
        $user->email = 'juandiego@gmail.com';
        $user->password = Hash::make('qwerty');
        $user->save();
        $user->roles()->attach(2);
    }
}

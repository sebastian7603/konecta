<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = 'ADMINISTRADOR';
        $role->save();

        $role = new Role();
        $role->name = 'VENDEDOR';
        $role->save();
    }
}

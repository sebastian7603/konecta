<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Auth routes
Route::prefix('/auth')->group(function () {
    Route::post('/login', 'AuthController@login');

    // Protected routes
    Route::middleware('auth.jwt')->group(function () {
        Route::post('/logout', 'AuthController@logout');
        Route::post('/refresh', 'AuthController@refresh');
        Route::post('/me', 'AuthController@me');
    });
});

// Roles route (Only the get method)
Route::get('/roles', 'RolesController');

// Protected routes
Route::middleware('auth.jwt')->group(function () {
    // Document types route (Only the get method)
    Route::get('/doc-types', 'DocTypesController');

    // Users routes
    Route::prefix('users')->middleware('role:1')->group(function () {
        Route::get('/', 'UsersController@index');
        Route::post('/store', 'UsersController@store');
        Route::get('/{id}', 'UsersController@show');
        Route::post('/{id}/edit', 'UsersController@update');
        Route::post('/{id}/delete', 'UsersController@destroy');
    });
    
    // Customers routes
    Route::prefix('customers')->middleware('role:1,2')->group(function () {
        Route::get('/', 'CustomersController@index');
        Route::post('/store', 'CustomersController@store');
        Route::get('/{id}', 'CustomersController@show');
        Route::post('/{id}/edit', 'CustomersController@update');
        Route::post('/{id}/delete', 'CustomersController@destroy');
    });
});
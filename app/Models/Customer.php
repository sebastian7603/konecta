<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

// Uso de "soft deleting" para no quitar los registros directamente de la base de datos
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'doc_type_id',
        'document',
        'names',
        'lastnames',
        'address',
        'email',
    ];

    /**
     * Get the document type that owns the customer.
     */
    public function docType()
    {
        return $this->belongsTo('App\Models\DocType', 'doc_type_id');
    }
}

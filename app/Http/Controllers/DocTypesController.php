<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\DocType;

class DocTypesController extends Controller
{
    public function __invoke () {
        $docTypes = DocType::all();
        return response()->json(compact('docTypes'), 200);
    }
}

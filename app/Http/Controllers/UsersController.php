<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\User;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('roles')->get();
        return response()->json(compact('users'), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        try {
            // We will use transactions for can do rollback if something is wrong
            DB::beginTransaction();
            $user->fill($request->all());
            $user->password = Hash::make($request->password);
            $user->save();
            $user->roles()->attach($request->roles);
            DB::commit();

            return response()->json(compact('user'), 201);
        } catch (\Exception $exc) {
            DB::rollback();
            report($exc);
            return response()->json(['message' => $exc->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('id', $id)->with('roles')->firstOrFail();
        return response()->json(compact('user'), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        try {
            // We will use transactions for can do rollback if something is wrong
            DB::beginTransaction();
            $user->fill($request->all());
            if ($request->password)
                $user->password = Hash::make($request->password);
            $user->save();
            if ($request->roles) {
                $user->roles()->sync($request->roles);
            } else {
                $user->roles()->detach();
            }
            $user->load('roles');
            DB::commit();
            
            return response()->json(compact('user'), 201);
        } catch (\Exception $exc) {
            DB::rollback();
            report($exc);
            return response()->json(['message' => $exc->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        try {
            // We will use transactions for can do rollback if something is wrong
            DB::beginTransaction();
            $user->delete();
            DB::commit();

            return response()->json(compact('user'), 200);
        } catch (\Exception $exc) {
            DB::rollback();
            report($exc);
            return response()->json(['message' => $exc->getMessage()], 400);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Customer;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::with('docType')->get();
        return response()->json(compact('customers'), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = new Customer();
        try {
            // We will use transactions for can do rollback if something is wrong
            DB::beginTransaction();
            $customer->fill($request->all());
            $customer->save();
            DB::commit();

            return response()->json(compact('customer'), 201);
        } catch (\Exception $exc) {
            DB::rollback();
            report($exc);
            return response()->json(['message' => $exc->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::where('id', $id)->with('docType')->firstOrFail();
        return response()->json(compact('customer'), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::findOrFail($id);
        try {
            // We will use transactions for can do rollback if something is wrong
            DB::beginTransaction();
            $customer->fill($request->all());
            $customer->save();
            $customer->load('docType');
            DB::commit();

            return response()->json(compact('customer'), 200);
        } catch (\Exception $exc) {
            DB::rollback();
            report($exc);
            return response()->json(['message' => $exc->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);
        try {
            // We will use transactions for can do rollback if something is wrong
            DB::beginTransaction();
            $customer->delete();
            DB::commit();

            return response()->json(compact('customer'), 200);
        } catch (\Exception $exc) {
            DB::rollback();
            report($exc);
            return response()->json(['message' => $exc->getMessage()], 400);
        }
    }
}

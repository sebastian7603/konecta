<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

use Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            $token = JWTAuth::attempt($credentials);
            if ($token && JWTAuth::user()->hasRole($request->role_id)) {
                $role_id = (int)$request->role_id;
                $token_type = 'bearer';
                $expiration = JWTAuth::factory()->getTTL() * 60;
                return response()->json(compact('token', 'token_type', 'expiration', 'role_id'), 200);
            }
            return response()->json(['message' => 'invalid_credentials'], 401);
        } catch (\Exception $exc) {
            return response()->json(['message' => $exc->getMessage()], 500);
        }
    }

    public function logout()
    {
        JWTAuth::invalidate();
        return response()->json(['message' => 'Successfully logged out'], 200);
    }

    public function refresh(Request $request)
    {
        try {
            $token = JWTAuth::refresh();
            if ($token && JWTAuth::user()->hasRole($request->header('role_id', 2))) {
                $role_id = (int)$request->header('role_id', 2);
                $token_type = 'bearer';
                $expiration = JWTAuth::factory()->getTTL() * 60;
                return response()->json(compact('token', 'token_type', 'expiration', 'role_id'), 200);
            }
            return response()->json(['message' => 'invalid_credentials'], 401);
        } catch (\Exception $exc) {
            return response()->json(['message' => $exc->getMessage()], 500);
        }
    }

    public function me()
    {
        $user = JWTAuth::user();
        return response()->json(compact('user'), 200);
    }
}

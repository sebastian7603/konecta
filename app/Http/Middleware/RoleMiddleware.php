<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        // Validate if role_id header exists
        if(!isset(apache_request_headers()['role_id']))
            return response()->json(['message' => 'Unauthorized'], 401);
        
        // Comparison of role_id header with authorized roles
        foreach($roles as $role){
            if (apache_request_headers()['role_id'] == $role) {
                return $next($request);
            }
        }

        return response()->json(['message' => 'Unauthorized'], 401);
    }
}

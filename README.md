# Konecta Laravel
Link demostrativo [https://youtu.be/6KbuNglWlIs](https://youtu.be/6KbuNglWlIs).


## Requisitos para deployar
- [Composer](https://getcomposer.org/).
- [Git](https://git-scm.com/).
- (Opcional) Empaquetado de desarrollo. Recomiendo [XAMPP](https://www.apachefriends.org/es/index.html).
- En caso de no usar un empaquetado de desarrollo, instalar un motor de BD.

## Repositorio del Front
[https://bitbucket.org/sebastian7603/konecta_front/src/master/](https://bitbucket.org/sebastian7603/konecta_front/src/master/).

## Pasos para deployar

### Ubicarse en la carpeta deseada y clonar el repositorio
```
git clone https://bitbucket.org/sebastian7603/konecta.git
```

### Ejecutar la instalación de paquetes
```
cd konecta
composer install
```

### Configurar .env
Se debe configurar en el .env:
- Datos de la Base de datos (utf-8).
- Link de la aplicación.
- Generar el key con el siguiente comando.
```
php artisan key:generate
```

### Ejecutar migraciones y seeds
```
php artisan migrate --seed
```

### Deplpoyar con servidor de artisan (*)
En caso de no usar XAMPP u otro empaquetado de herramientas, ejecutar el servidor del artisan.
```
php artisan serve
```
